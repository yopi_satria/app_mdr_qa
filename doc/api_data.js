define({ "api": [
  {
    "type": "post",
    "url": "/api/v1/auth/login",
    "title": "1. Login dan getToken",
    "version": "0.1.0",
    "name": "login",
    "group": "Authentication",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk cek username dan password, dan jika berhasil maka akan melakukan generate Token.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Required Username dari user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Required Password dari user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "manufacture",
            "description": "<p>Optional.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "model",
            "description": "<p>Optional.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>Optional.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sdk",
            "description": "<p>Optional.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n\t\"username\": \"rano.dts\"\n\t\"password\": \"libranofauzan\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.nama",
            "description": "<p>Nama user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.email",
            "description": "<p>Email user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.jabatan",
            "description": "<p>Jabatan user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.account_name",
            "description": "<p>Account name user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.account_type",
            "description": "<p>Account type user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.username",
            "description": "<p>Username user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.token",
            "description": "<p>Token key user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t\"nama\": \"Librano Fauzan\",\n\t\t\"email\": \"rano.dts@gmf-aeroasia.co.id\",\n\t\t\"jabatan\": \"3rd Party - Programmer\",\n\t\t\"account_name\": \"rano.dts\",\n\t\t\"account_type\": \"805306368\",\n\t\t\"username\": \"rano.dts\",\n\t\t\"token\": \"c3130467372ddb6a190bb873c3a73263\"\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Akun LDAP tidak ada!\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Auth.php",
    "groupTitle": "Authentication",
    "sampleRequest": [
      {
        "url": "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/auth/login"
      }
    ]
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./apidoc/main.js",
    "group": "C__xampp_htdocs_GMF_mdr_api_apidoc_main_js",
    "groupTitle": "C__xampp_htdocs_GMF_mdr_api_apidoc_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "C__xampp_htdocs_GMF_mdr_api_doc_main_js",
    "groupTitle": "C__xampp_htdocs_GMF_mdr_api_doc_main_js",
    "name": ""
  },
  {
    "type": "post",
    "url": "/api/v1/config/set-config_path",
    "title": "1. Setting Config Path",
    "version": "0.1.0",
    "name": "set_ConfigPath",
    "group": "Configuration",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk setting path folder upload data</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>Required Path folder.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Required Type Path folder.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"path\": your path folder\n\t\"type\": your type path (image/APK/.est)\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Config.php",
    "groupTitle": "Configuration",
    "sampleRequest": [
      {
        "url": "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/config/set-config_path"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/v1/mdr/create-mdr",
    "title": "4. Create Transaksi MDR",
    "version": "0.1.0",
    "name": "create_Mdr",
    "group": "Function_SAP",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk create transaksi MDR via aplikasi mobile</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "orig_order",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mdr_desc",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "op_text",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mdr_subc",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_psessi",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_sta_type",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_sta_from",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_sta_to",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_str_from",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_str_to",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_wl_from",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_wl_to",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_bl_from",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_bl_to",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_zone",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_clk_pst",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_defect_limit",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_urgrp",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_urcod",
            "description": "<p>Required</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "smr_urtxt",
            "description": "<p>Required</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.id",
            "description": "<p>ID Transaksi.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t\"id\": \"xx\",\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Mdr.php",
    "groupTitle": "Function_SAP",
    "sampleRequest": [
      {
        "url": "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/mdr/create-mdr"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/v1/mdr/get-image/:nama_file",
    "title": "3. Get View Image",
    "version": "0.1.0",
    "name": "get_Image",
    "group": "Function_SAP",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk menampilkan gambar yang sudah diupload sebelumnya</p>",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n\t\"nama_file\": image.jpg\n}",
          "type": "string"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Mdr.php",
    "groupTitle": "Function_SAP"
  },
  {
    "type": "post",
    "url": "/api/v1/mdr/get-order",
    "title": "1. Get Order",
    "version": "0.1.0",
    "name": "get_Order",
    "group": "Function_SAP",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data Order sesuai dengan nomor order yang dikirimkan</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "order",
            "description": "<p>Required Order number parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"order\": your order number\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.kode_order",
            "description": "<p>kode order untuk melihat ada atau tidaknya data order di SAP.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"kode_order\": \"order valid\",\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Mdr.php",
    "groupTitle": "Function_SAP",
    "sampleRequest": [
      {
        "url": "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/mdr/get-order"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/v1/mdr/upload-image",
    "title": "2. Upload Image",
    "version": "0.1.0",
    "name": "upload_Image",
    "group": "Function_SAP",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengupload data Image</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "Files",
            "optional": false,
            "field": "image",
            "description": "<p>Required Data image file.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "transaction_id",
            "description": "<p>Required ID Transaction.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"image\": your image file\n\t\"transaction_id\" : your id transaction\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.kode",
            "description": "<p>kode untuk melihat sukses atau tidaknya saat melakukan upload image.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"kode\": \"upload image sukses\",\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Mdr.php",
    "groupTitle": "Function_SAP"
  },
  {
    "type": "post",
    "url": "/api/v1/master/get-cause_code",
    "title": "1. Get Cause Code",
    "version": "0.1.0",
    "name": "get_CauseCode",
    "group": "Master",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data Cause Code dari database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "select[]",
            "defaultValue": "*",
            "description": "<p>Optional select parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "where[]",
            "defaultValue": "null",
            "description": "<p>Optional where condition parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"select\": column name, column name\n\t\"where\": column name = foo, column name like %foo%\n}\nin Script PHP\n<php\n\t$select = array(\"column name\",\"column name\");\n\t$where = array(\"column name='foo'\",\"column name like %foo%\");\n?>",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.column_name",
            "description": "<p>name Data dari kolom yang dipilih.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"id_cause\": \"ZPM-STR_AC\",\n\t\t\t\"group_code\": \"ZPM-STR\",\n\t\t\t\"subcause\": \"AC\",\n\t\t\t\"desc_causecode\": \"Accidental damage\"\n\t\t},\n\t\t{\n\t\t\t\"id_cause\": \"ZPM-STR_BL\",\n\t\t\t\"group_code\": \"ZPM-STR\",\n\t\t\t\"subcause\": \"BL\",\n\t\t\t\"desc_causecode\": \"Blockage\"\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Master.php",
    "groupTitle": "Master",
    "sampleRequest": [
      {
        "url": "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/master/get-cause_code"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/v1/master/get-mdr_code",
    "title": "2. Get MDR Code",
    "version": "0.1.0",
    "name": "get_MdrCode",
    "group": "Master",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data MDR Code dari database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "select[]",
            "defaultValue": "*",
            "description": "<p>Optional select parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "where[]",
            "defaultValue": "null",
            "description": "<p>Optional where condition parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"select\": column name, column name\n\t\"where\": column name = foo, column name like %foo%\n}\nin Script PHP\n<php\n\t$select = array(\"column name\",\"column name\");\n\t$where = array(\"column name='foo'\",\"column name like %foo%\");\n?>",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.column_name",
            "description": "<p>name Data dari kolom yang dipilih.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"code\": \"ZPM-AVI\",\n\t\t\t\"desc\": \"Avionic Defect Codes\"\n\t\t},\n\t\t{\n\t\t\t\"code\": \"ZPM-CMC\",\n\t\t\t\"desc\": \"Complex Maintenance Codes\"\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Master.php",
    "groupTitle": "Master",
    "sampleRequest": [
      {
        "url": "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/master/get-mdr_code"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/v1/master/get-station_type",
    "title": "3. Get Station Type",
    "version": "0.1.0",
    "name": "get_StationType",
    "group": "Master",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data Station Type dari database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "select[]",
            "defaultValue": "*",
            "description": "<p>Optional select parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "where[]",
            "defaultValue": "null",
            "description": "<p>Optional where condition parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"select\": column name, column name\n\t\"where\": column name = foo, column name like %foo%\n}\nin Script PHP\n<php\n\t$select = array(\"column name\",\"column name\");\n\t$where = array(\"column name='foo'\",\"column name like %foo%\");\n?>",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.column_name",
            "description": "<p>name Data dari kolom yang dipilih.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"tipe_station\": \"AE\",\n\t\t\t\"desc\": \"Aileron\"\n\t\t},\n\t\t{\n\t\t\t\"code\": \"BBL\",\n\t\t\t\"desc\": \"Body Buttock Line\"\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Master.php",
    "groupTitle": "Master",
    "sampleRequest": [
      {
        "url": "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/master/get-station_type"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/v1/master/get-sub_code",
    "title": "4. Get Sub Code",
    "version": "0.1.0",
    "name": "get_SubCode",
    "group": "Master",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>digunakan untuk mengambil data Sub Code dari database</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Required Token for use this API.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "select[]",
            "defaultValue": "*",
            "description": "<p>Optional select parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "where[]",
            "defaultValue": "null",
            "description": "<p>Optional where condition parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "in Try Out\n{\n\t\"token\": your token\n\t\"select\": column name, column name\n\t\"where\": column name = foo, column name like %foo%\n}\nin Script PHP\n<php\n\t$select = array(\"column name\",\"column name\");\n\t$where = array(\"column name='foo'\",\"column name like %foo%\");\n?>",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codestatus",
            "description": "<p>Response Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Response Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array[]",
            "optional": false,
            "field": "resultdata",
            "description": "<p>Response Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "resultdata.column_name",
            "description": "<p>name Data dari kolom yang dipilih.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"codestatus\": \"S\",\n\t\"message\": \"Sukses\",\n\t\"resultdata\": {\n\t\t{\n\t\t\t\"id_subcode\": \"ZPM-AVI_AC\",\n\t\t\t\"group_code\": \"ZPM-AVI\",\n\t\t\t\"subcode\": \"AC\",\n\t\t\t\"desc_subcode\" \"Arcing-connector\"\n\t\t},\n\t\t{\n\t\t\t\"id_subcode\": \"ZPM-AVI_AS\",\n\t\t\t\"group_code\": \"ZPM-AVI\",\n\t\t\t\"subcode\": \"AS\",\n\t\t\t\"desc_subcode\" \"Arcing-splice\"\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n\t\"codestatus\": \"E\",\n\t\"message\": \"Error Message\",\n\t\"resultdata\": [],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/controller/Master.php",
    "groupTitle": "Master",
    "sampleRequest": [
      {
        "url": "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/master/get-sub_code"
      }
    ]
  }
] });
