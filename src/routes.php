<?php
use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

/*Routing Testing*/
$app->any('/api/helo', function (Request $request, Response $response, array $args) {
    return $this->api->helo();
});
$app->any('/api/sapa/{kalimat}/{nama}', function (Request $request, Response $response, array $args) {
    return $this->api->sapa($args['kalimat'],$args['nama']);
});
$app->any('/api/get', function (Request $request, Response $response, array $args) {
    //kirim koneksi ke controller
    $this->api->koneksi($this->db);
    //balikan json dari controller
    return $response->withJson($this->api->getData(),200);
});



/*Routing MDR*/
$app->group('/api/v1/auth', function () use($app) {
    $app->post('/login', function (Request $request, Response $response, array $args) {
        $this->auth->koneksi($this->db);
        return $response->withJson($this->auth->login(),200);
    });
});


$app->group('/api/v1/config', function () use($app) {
    $app->post('/set-config_path', function (Request $request, Response $response, array $args) {
        $this->config->koneksi($this->db);
        return $response->withJson($this->config->set_ConfigPath(),200);
    });
});


$app->group('/api/v1/master', function () use($app) {
    $app->post('/get-cause_code', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm,$this->urlprefix_image);
        return $response->withJson($this->master->get_CauseCode(),200);
    });

    $app->post('/get-mdr_code', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm,$this->urlprefix_image);
        return $response->withJson($this->master->get_MdrCode(),200);
    });

    $app->post('/get-station_type', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm,$this->urlprefix_image);
        return $response->withJson($this->master->get_StationType(),200);
    });

    $app->post('/get-sub_code', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm,$this->urlprefix_image);
        return $response->withJson($this->master->get_SubCode(),200);
    });
	
	/*  
	 * Module Summary 
	 */
	$app->post('/get-aircraft', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm);
        return $response->withJson($this->master->get_Aircraft(),200);
    });
	$app->post('/summary-list', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm);
        return $response->withJson($this->master->get_MdrList(),200);
    });

	$app->post('/summary-detail', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm);
        return $response->withJson($this->master->get_MdrImage(),200);
    });
	
	/*  
	 * Create Summary 
	 */
	$app->post('/get-plant', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm);
        return $response->withJson($this->master->get_Plant(),200);
    });
	$app->post('/get-workcenter', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm);
        return $response->withJson($this->master->get_Workcenter(),200);
    });
});


$app->group('/api/v1/mdr', function () use($app) {   
	
	$app->post('/get-order', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap,
							$this->wsdl, $this->urlprefix_image,
							$this->user_ftp);
        return $response->withJson($this->mdr->get_Order(),200);
    });

    $app->post('/upload-image', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap, 
							$this->wsdl, $this->urlprefix_image,
							$this->user_ftp);
        return $response->withJson($this->mdr->upload_Image(),200);
    });

    $app->get('/get-image/[{image}]', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap, 
							$this->wsdl, $this->urlprefix_image,
							$this->user_ftp);
        $response->getBody()->write($this->mdr->get_Image($args['image']),200);
        $response = $response->withHeader('Content-type', 'image/jpeg'); 
        return $response;
    });

    $app->get('/get-image-url/[{image}]', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap, 
							$this->wsdl, $this->urlprefix_image,
							$this->user_ftp);
        $response->getBody()->write($this->mdr->get_ImageUrl($args['image']),200);
        $response = $response->withHeader('Content-type', 'image/jpeg'); 
        return $response;
    });

    $app->post('/create-mdr', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap, 
							$this->wsdl, $this->urlprefix_image,
							$this->user_ftp);       
		return $response->withJson($this->mdr->create_Notif(),200);
		// return $response->withJson($this->mdr->create_Mdr(),200);		
    });
});


//Menggunakan BAPI
$app->group('/api/v2/mdr', function () use($app) {   
	$app->post('/create-mdr', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap, 
							$this->wsdl, $this->urlprefix_image,
							$this->user_ftp);       
		
		return $response->withJson($this->mdr->create_Mdr(),200);		
    });
});


/* Test Routing MDR
 * Local to DEV
 *
*/
$app->group('/local/v1/auth', function () use($app) {
    $app->post('/login', function (Request $request, Response $response, array $args) {
        $this->auth->koneksi($this->db);
        return $response->withJson($this->auth->login(),200);
    });
});

$app->group('/local/v1/master', function () use($app) {
	$app->post('/get-aircraft', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm);
        return $response->withJson($this->master->get_Aircraft(),200);
    });
    $app->post('/summary-list', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm);
        return $response->withJson($this->master->get_MdrList(),200);
    });

	$app->post('/summary-detail', function (Request $request, Response $response, array $args) {
        $this->master->koneksi($this->db,$this->db_crm);
        return $response->withJson($this->master->get_MdrImage(),200);
    });
});

$app->group('/local/v1/mdr', function () use($app) {
	
    $app->post('/get-order', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap, $this->wsdl,
							$this->urlprefix_image,$this->user_ftp);
        return $response->withJson($this->mdr->get_Order(),200);
    });

    $app->post('/upload-image', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap, $this->wsdl,
							$this->urlprefix_image,$this->user_ftp);
        return $response->withJson($this->mdr->upload_Image(),200);
    });

    $app->get('/get-image/[{image}]', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap, $this->wsdl,
							$this->urlprefix_image,$this->user_ftp);
        $response->getBody()->write($this->mdr->get_Image($args['image']),200);
        $response = $response->withHeader('Content-type', 'image/jpeg'); 
        return $response;
    });

    $app->post('/create-mdr', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap, $this->wsdl,
							$this->urlprefix_image,$this->user_ftp);
        //return $response->withJson($this->mdr->create_Mdr(),200);
		return $response->withJson($this->mdr->create_Notif(),200);
		
    });

});

/* Test Routing MDR
 * No Connection Dbase
*/
$app->group('/hard/v1/auth', function () use($app) {
    $app->post('/login', function (Request $request, Response $response, array $args) {
        //$this->auth->koneksi($this->db);
		$resp['codestatus'] = "S";
		$resp['message'] = "Sukses";
		$resp['resultdata'] = array(						
								"nama" => "Librano Fauzan",
								"email"=> "rano.dts@gmf-aeroasia.co.id",
								"jabatan"=> "3rd Party - Programmer",
								"account_name"=> "rano.dts",
								"account_type"=> "805306368",
								"username"=> "rano.dts",
								"token"=> "b2f6b5d00ed9d42e59b146db5483ebe4",
								"manufacture"=> "Samsung",
								"model"=> "Galaxy S",
								"version"=> "6.0.1",
								"sdk_version"=> "Marsmallow"							
							);
		
        return $response->withJson($resp,200);
    });
});

$app->group('/hard/v1/mdr', function () use($app) {
	
    $app->post('/get-order', function (Request $request, Response $response, array $args) {        
		$resp['codestatus'] = "S";
		$resp['message'] = "Sukses";
		$resp['resultdata'] = array(	
							"BSC_STRDATE"=> "26.06.2018",
							"FLOC"=> "PK-GFM",
							"MAT"=> "RST",
							"M_WORK_CENTER"=> "GAH401ZZ",
							"ORDER_TITLE"=> "Test GTL Tally Sheet.",
							"PM_PS"=> "P-73N-200-10-4H-00/100",
							"WORK_CENTER"=> "GAH4",
							"REVISION"=> "00004642",
							"LOCATE" => __DIR__
						);
        return $response->withJson($resp,200);
    });

    $app->post('/upload-image', function (Request $request, Response $response, array $args) {
		$master_gambar = $_FILES['image'];
		if(!isset($master_gambar)) throw new \InvalidArgumentException("Format image salah!", 1);
		if(empty($master_gambar)) throw new \InvalidArgumentException("Format image salah!", 1);
		
		//echo "<pre>"; print_r($master_gambar);echo "</pre>";die();
		//$nama_gambar = str_replace(" ", "_", $master_gambar['name']);
		//$nama_gambar = $master_gambar['name'];
		$nama_gambar = $_POST['nama'];
		$mdrno = $_POST['mdr_order'];
		$format_gambar = explode(".", $nama_gambar);
		$format_gambar = $format_gambar[1];
		$tmp_gambar = $master_gambar['tmp_name'];
		
		$format_nama_gambar = $nama_gambar;
		//$destination = $this->path_image;
		$destination = __DIR__."\..\assets\image\\";
		chown($destination, 0755);
		$url_image = "http://dev.gmf-aeroasia.co.id/app_mdr/public/index.php/api/v1/mdr/get-image/".$format_nama_gambar;
		//echo $destination;die();
		$upload = move_uploaded_file($tmp_gambar, $destination.$format_nama_gambar);
		
		$resp['codestatus'] = "S";
		$resp['message'] = "Sukses Upload Image";
		$resp['resultdata'] = array(	
							'TRANS_ID' => 45
						);
				
        return $response->withJson($resp,200);
    });

    $app->get('/get-image/[{image}]', function (Request $request, Response $response, array $args) {
        $this->mdr->koneksi($this->db,$this->soap);
        $response->getBody()->write($this->mdr->get_Image($args['image']),200);
        $response = $response->withHeader('Content-type', 'image/jpeg'); 
        return $response;
    });

    $app->post('/create-mdr', function (Request $request, Response $response, array $args) {
       
		$resp['codestatus'] = "S";
		$resp['message'] = "Sukses";
		$resp['resultdata'] = array(	
							'AUFNR_NEW' => "8058222222",
							'AUFNR_TMP' => "8040111111",
							'QMNUM' => "100003238",
							'TRANS_ID' => "45"
						);
        return $response->withJson($resp,200);
    });
});

$app->group('/hard/v1/master', function () use($app) {
    $app->post('/summary-list', function (Request $request, Response $response, array $args) {
        //$this->master->koneksi($this->db);
		$resp['codestatus'] = "S";
		$resp['message'] = "Sukses";
		$resp['resultdata'] = array(	
									 [
										"revnr"=> "35122",
										"mdrorder"=> "801407609",
										"origorder"=> "800030234",
										"revtx"=> "PK-CLU B737-500 SRIWIJAYA C04-CHK DEC 16",
										"tplnr"=> "PK-CLU",
										"mdrdesc"=> "AFT CARGO DOOR LANYARD BROKEN",
										"mdrdate"=> "06-01-2017",
										"ernam"=> "S165018",
										"mdrstatus"=> "CLOSE",
										"sapstatus"=> "CLOSE"
									],
									[
										"revnr"=> "35122",
										"mdrorder"=> "801407609",
										"origorder"=> "800030224",
										"revtx"=> "PK-CLU B737-500 SRIWIJAYA C04-CHK DEC 16",
										"tplnr"=> "PK-CLU",
										"mdrdesc"=> "AFT CARGO DOOR BLANKET TEAR OFF",
										"mdrdate"=> "06-01-2017",
										"ernam"=> "S165018",
										"mdrstatus"=> "OPEN",
										"sapstatus"=> "OPEN"
									],
									[
										"revnr"=> "35122",
										"mdrorder"=> "801407606",
										"origorder"=> "800040234",
										"revtx"=> "PK-CLU B737-500 SRIWIJAYA C04-CHK DEC 16",
										"tplnr"=> "PK-CLU",
										"mdrdesc"=> "LOCK OF FCCA, ADC 1 AND SMC 2 BROKEN",
										"mdrdate"=> "02-01-2017",
										"ernam"=> "S165016",
										"mdrstatus"=> "PROGRESS",
										"sapstatus"=> "PROGRESS"
									],
									[
										"revnr"=> "35122",
										"mdrorder"=> "801407606",
										"origorder"=> "800020845",
										"revtx"=> "PK-CLU B737-500 SRIWIJAYA C04-CHK DEC 16",
										"tplnr"=> "PK-CLU",
										"mdrdesc"=> "E1, E2 AND E3 RACK DIRTY",
										"mdrdate"=> "02-01-2017",
										"ernam"=> "S165016",
										"mdrstatus"=> "PENDING",
										"sapstatus"=> "PENDING"
									]									
								);
        return $response->withJson($resp,200);
    });
	$app->post('/summary-detail', function (Request $request, Response $response, array $args) {
        //$this->master->koneksi($this->db);
		$resp['codestatus'] = "S";
		$resp['message'] = "Sukses";
		$resp['resultdata'] = array(	
									'revnr' => "0004642",
									'tplnr' => "PK-GLF",
									'mdr_order' => "800436262",
									'mdr_desc' => "lead bonding broken",
									'orig order' => "800434111",
									'created' => "2018-08-31 13:38:24.000",
									'image' => array(
											"http://10.0.2.2/api-mdr-mobile/assets/image/g324242_2018010234.jpg",
											"http://10.0.2.2/api-mdr-mobile/assets/image/rano.dts_20180901223829512138996.jpg",
											"http://10.0.2.2/api-mdr-mobile/assets/image/rano.dts_20180901223829512806704.jpg"
											)
								);
        return $response->withJson($resp,200);
    });
	$app->post('/get-plant', function (Request $request, Response $response, array $args) {
        //$this->master->koneksi($this->db);
		$resp['codestatus'] = "S";
		$resp['message'] = "Sukses";
		$resp['resultdata'] = [
								['werks' => "WSAV",
									'name' => "Avionic Workshop",
									'city' => "Tangerang",									
								],
								['werks' => "WSCA",
									'name' => "WS H5 - CABIN MONUMENT WORKSHO",
									'city' => "Tangerang",									
								],
								['werks' => "WSCB",
									'name' => "Cabin Furnishing Workshop",
									'city' => "Tangerang",									
								],
								['werks' => "WSCN",
									'name' => "WS H4 - Cabin Monument Shop",
									'city' => "Tangerang",									
								],
		
							   ];
        return $response->withJson($resp,200);
    });
	$app->post('/get-workcenter', function (Request $request, Response $response, array $args) {
        //$this->master->koneksi($this->db);
		$resp['codestatus'] = "S";
		$resp['message'] = "Sukses";
		$resp['resultdata'] = [
								[	'arbpl' => "WBLGPLLG",
									'werks' => "WSEM",
									'name' => "WS Gas",									
								],
								[	'arbpl' => "WBLGPLZZ",
									'werks' => "WSEM",
									'name' => "WS Emergency",									
								],
								[	'arbpl' => "WBLGRPLG",
									'werks' => "WSEM",
									'name' => "WS Fuel",									
								],
								[	'arbpl' => "WBLGRPZZ",
									'werks' => "WSLS",
									'name' => "WS Laundry",									
								],
		
							   ];
        return $response->withJson($resp,200);
    });

});
