<?php
namespace MdrApp\Model;

class M_auth extends \Slim\App{

	protected $con;
	protected $tb_login = "TBL_MDR_MOBILE_LOGIN_LOG";

	public function __construct($key)
    {
        $this->con = $key;
    }

	public function update($param,$where = []){
		try{
			if(!is_array($param)) throw new \InvalidArgumentException("Param required is array", 1);

			if(count($param) == 0){
				throw new \InvalidArgumentException("Param is required", 1);
			}else{
				$param_string = "";
				foreach ($param as $key => $value) {
					$param_string .= $key . "='" . $value . "' , ";
				}
				$param_string = rtrim($param_string,' , ');
			}

			if(count($where) == 0){
				$where_string = "";
			}else{
				$where_string = "WHERE ";
				foreach ($where as $key => $value) {
					$where_string .= $key . "='" . $value . "' AND ";
				}
				$where_string = rtrim($where_string,' AND ');
			}

			$sql = "UPDATE $this->tb_login SET $param_string $where_string";
			$stmt = $this->con->prepare($sql);
			$result = $stmt->execute();
			
			return $result;
		}catch(\InvalidArgumentException $e){	
			return FALSE;
	    }
	}

	public function insert($param){
		try{
			if(!is_array($param)) throw new \InvalidArgumentException("Param required is array", 1);

			if(count($param) == 0){
				throw new \InvalidArgumentException("Param is required", 1);
			}else{
				$param_set = "";
				$param_value = "";
				foreach ($param as $key => $value) {
					$param_set .= $key . ", ";
					$param_value .= "'" . $value . "', ";
				}
				$param_set = rtrim($param_set,' ,');
				$param_value = rtrim($param_value,' ,');
			}

			$sql = "INSERT INTO $this->tb_login ($param_set) VALUES ($param_value)";
			$stmt = $this->con->prepare($sql);
			$result = $stmt->execute();
			
			return $result;
		}catch(\InvalidArgumentException $e){	
			return FALSE;
	    }
	}

	public function query($sql){
		try{
			$stmt = $this->con->prepare($sql);
			$stmt->execute();
			$result = $stmt->fetchAll();
			
			return $result;
		}catch(\PDOExeption $e){
			return $e->getMessage();
	    }
	}
}
?>