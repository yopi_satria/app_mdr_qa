<?php
namespace MdrApp\Model;

class Data extends \Slim\App{

	protected $con;

	function PDOcon($conn)
	{
		$this->con = $conn;
	}

	public function query($koneksi,$sql){
		try{
			$stmt = $koneksi->prepare($sql);
			$stmt->execute();
			$result = $stmt->fetchAll();
			$respon_field["codestatus"] = 'S';
			$respon_field["message"] = "Sukses";
			$respon_field["resultdata"] = $result;
			
			return $respon_field;
		}catch(\PDOExeption $e){	
			$respon_field["codestatus"] = 'E';
			$respon_field["message"] = "Gagal";

			return $respon_field;
	    }
	}

	public function insert($koneksi,$sql){
		try{
			$stmt = $koneksi->prepare($sql);
			$result = $stmt->execute();
			$respon["codestatus"] = 'S';
			$respon["message"] = "Sukses";
			$respon["resultdata"] = $result;
			
			return $respon;
		}catch(\PDOExeption $e){	
			$respon_field["codestatus"] = 'E';
			$respon_field["message"] = "Gagal";
			$respon_field["resultdata"] = [];

			return $respon_field;
	    }
	}
}
?>