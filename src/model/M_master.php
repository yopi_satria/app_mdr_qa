<?php
namespace MdrApp\Model;

class M_master extends \Slim\App{

	protected $con;

	public function __construct($key)
    {
        $this->con = $key;
    }

    public function select($param=[], $where=[], $table)
    {
    	try {
    		if(count($param) == 0){
				$select_string = "*";
			}else{
				$select_string = "";
				foreach ($param as $key => $value) {
					$select_string .= $value.", ";
				}
				$select_string = rtrim($select_string,' ,');
			}

			if(count($where) == 0){
				$where_string = "";
			}else{
				$where_string = "WHERE ";
				foreach ($where as $key => $value) {
					$where_string .= $key . "='" . $value . "' AND ";
				}
				$where_string = rtrim($where_string,' AND ');
			}

			if(empty($table)) throw new \InvalidArgumentException("Table is required", 1);
			

			$sql = "SELECT $select_string FROM $table $where_string";
			$stmt = $this->con->prepare($sql);
			$result = $stmt->execute();
			$result = $stmt->fetchAll();
			$respon["codestatus"] = 'S';
			$respon["message"] = "Sukses";
			$respon["resultdata"] = $result;
			
			return $respon;
		}catch(\InvalidArgumentException $e){	
			$respon_field["codestatus"] = 'E';
			$respon_field["message"] = $e->getMessage();

			return $respon_field;
	    }
    }

    public function select_master($param=[], $where, $table)
    {
    	try {
    		if(count($param) == 0){
				$select_string = "*";
			}else{
				$select_string = "";
				foreach ($param as $key => $value) {
					$select_string .= $value.", ";
				}
				$select_string = rtrim($select_string,' ,');
			}

			if(empty($table)) throw new \InvalidArgumentException("Table is required", 1);
			

			$sql = "SELECT $select_string FROM $table $where";
			$stmt = $this->con->prepare($sql);
			$result = $stmt->execute();
			$result = $stmt->fetchAll();
			$respon["codestatus"] = 'S';
			$respon["message"] = "Sukses";
			$respon["resultdata"] = $result;
			
			return $respon;
		}catch(\InvalidArgumentException $e){	
			$respon_field["codestatus"] = 'E';
			$respon_field["message"] = $e->getMessage();

			return $respon_field;
	    }
    }

	public function query($sql){
		try{
			$stmt = $this->con->prepare($sql);
			$stmt->execute();
			$result = $stmt->fetchAll();
			
			return $result;
		}catch(\PDOExeption $e){
			return $e->getMessage();
	    }
	}
}
?>