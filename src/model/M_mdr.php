<?php
namespace MdrApp\Model;

class M_mdr extends \Slim\App{

	protected $con;

	public function __construct($key)
    {
        $this->con = $key;
    }

    public function select($param=[], $where=[], $table)
    {
    	try {
    		if(count($param) == 0){
				$select_string = "*";
			}else{
				$select_string = "";
				foreach ($param as $key => $value) {
					$select_string .= $value.", ";
				}
				$select_string = rtrim($select_string,' ,');
			}

			if(count($where) == 0){
				$where_string = "";
			}else{
				$where_string = "WHERE ";
				foreach ($where as $key => $value) {
					$where_string .= $key . "='" . $value . "' AND ";
				}
				$where_string = rtrim($where_string,' AND ');
			}

			if(empty($table)) throw new \InvalidArgumentException("Table is required", 1);
			

			$sql = "SELECT $select_string FROM $table $where_string";
			$stmt = $this->con->prepare($sql);
			$result = $stmt->execute();
			$result = $stmt->fetchAll();
			$respon["codestatus"] = 'S';
			$respon["message"] = "Sukses";
			$respon["resultdata"] = $result;
			
			return $respon;
		}catch(\InvalidArgumentException $e){	
			$respon_field["codestatus"] = 'E';
			$respon_field["message"] = $e->getMessage();

			return $respon_field;
	    }
    }

    public function select_distinct($param=[], $where=[], $table)
    {
    	try {
    		if(count($param) == 0){
				$select_string = "*";
			}else{
				$select_string = "";
				foreach ($param as $key => $value) {
					$select_string .= $value.", ";
				}
				$select_string = rtrim($select_string,' ,');
			}

			if(count($where) == 0){
				$where_string = "";
			}else{
				$where_string = "WHERE ";
				foreach ($where as $key => $value) {
					$where_string .= $key . "='" . $value . "' AND ";
				}
				$where_string = rtrim($where_string,' AND ');
			}

			if(empty($table)) throw new \InvalidArgumentException("Table is required", 1);
			

			$sql = "SELECT DISTINCT $select_string FROM $table $where_string";
			$stmt = $this->con->prepare($sql);
			$result = $stmt->execute();
			$result = $stmt->fetchAll();
			$respon["codestatus"] = 'S';
			$respon["message"] = "Sukses";
			$respon["resultdata"] = $result;
			
			return $respon;
		}catch(\InvalidArgumentException $e){	
			$respon_field["codestatus"] = 'E';
			$respon_field["message"] = $e->getMessage();

			return $respon_field;
	    }
    }

    public function insert($param,$table){
		try{
			if(!is_array($param)) throw new \InvalidArgumentException("Param required is array", 1);
			if(empty($table)) throw new \InvalidArgumentException("Table name is required", 1);

			if(count($param) == 0){
				throw new \InvalidArgumentException("Param is required", 1);
			}else{
				$param_set = "";
				$param_value = "";
				foreach ($param as $key => $value) {
					$param_set .= $key . ", ";
					$param_value .= "'" . $value . "', ";
				}
				$param_set = rtrim($param_set,' ,');
				$param_value = rtrim($param_value,' ,');
			}
//echo $param_value;
			$sql = "INSERT INTO $table ($param_set) VALUES ($param_value);SELECT SCOPE_IDENTITY() AS ID_TRANS;";
			
			$stmt = $this->con->prepare($sql);
			$result = $stmt->execute();
			$result = $stmt->nextRowset();
			$result = $stmt->fetchAll();

			$respon = [
				'id' => $result[0]['ID_TRANS'],
			];
			
			return $respon;
		}catch(\InvalidArgumentException $e){	
			return FALSE;
	    }
	}

	public function query($sql){
		try{
			$stmt = $this->con->prepare($sql);
			$stmt->execute();
			$result = $stmt->fetchAll();
			
			return $result;
		}catch(\PDOExeption $e){
			return $e->getMessage();
	    }
	}
}
?>