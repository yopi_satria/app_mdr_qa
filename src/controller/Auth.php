<?php
namespace MdrApp\Controller;
use MdrApp\Model\M_auth;
date_default_timezone_set("Asia/Jakarta");

class Auth
{
	protected $con;
	protected $M_auth;

	public function koneksi($conn)
	{
		$this->con = $conn;
		$this->M_auth = new M_auth($conn);
	}

	public function cek_ldap($username,$password,$ip)
	{
		$dn = "DC=gmf-aeroasia,DC=co,DC=id";
		//IP 1 = 192.168.240.57
		//IP 2 = 172.16.100.46
        $ldapconn = ldap_connect($ip) or die ("Could not connect to LDAP server.");
        if ($ldapconn) {
            ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
            $ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
            @$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
            @$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
            @$info = ldap_get_entries($ldapconn, @$sr);
            @$infomail = ldap_get_entries($ldapconn, @$srmail);
            @$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
            @$bind = @ldap_bind($ldapconn, $info[0][dn], $password);
            //print("<pre>".print_r(@$info,true)."</pre>");die();
            //print("<pre>".print_r(@$infomail,true)."</pre>");
            //print("<pre>".print_r(@$usermail,true)."</pre>");
            //print("<pre>".print_r(@$bind,true)."</pre>");
            //die();
            if ((@$info[0]["samaccountname"][0] == $username AND $bind) OR (@$usermail == $username AND $bind)) {
				//echo "<pre>".print_r($info)."</pre>";die();
            	$hasil = [
            		'status' => true,
            		'nama' => @$info[0]['displayname'][0],
            		'email' => @$info[0]['mail'][0],
            		'jabatan' => @$info[0]['description'][0],
            		'account_name' => @$info[0]['samaccountname'][0],
            		'account_type' => @$info[0]['samaccounttype'][0],
            	];
                return $hasil;
            } else {
            	$hasil = [
            		'status' => false,
            	];
                return $hasil;
            }
        } else {
        	$hasil = [
            	'status' => false,
            	'message' => "LDAP Connection trouble, please try again 2/3 time",
        	];
            return $hasil;
            //echo "LDAP Connection trouble,, please try again 2/3 time";
        }
	}

	/**
	* @api {post} /api/v1/auth/login 1. Login dan getToken
	* @apiVersion 0.1.0
	* @apiName login
	* @apiGroup Authentication
	* @apiPermission public
	* @apiDescription digunakan untuk cek username dan password, dan jika berhasil maka akan melakukan generate Token.
	*
	*
	* @apiParam {String} username  Required Username dari user.
	* @apiParam {String} password  Required Password dari user.
	* @apiParam {String} manufacture  Optional.
	* @apiParam {String} model  Optional.
	* @apiParam {String} version  Optional.
	* @apiParam {String} sdk  Optional.
	*
	* @apiParamExample {json} Request-Example:
	* {
	*	"username": "rano.dts"
	*	"password": "libranofauzan"
	* }
	*
	*
	* @apiSuccess {String} codestatus Response Status.
	* @apiSuccess {String} message Response Message.
	* @apiSuccess {Array[]} resultdata Response Data.
	* @apiSuccess {String} resultdata.nama Nama user.
	* @apiSuccess {String} resultdata.email Email user.
	* @apiSuccess {String} resultdata.jabatan Jabatan user.
	* @apiSuccess {String} resultdata.account_name Account name user.
	* @apiSuccess {String} resultdata.account_type Account type user.
	* @apiSuccess {String} resultdata.username Username user.
	* @apiSuccess {String} resultdata.token Token key user.
	*
	* @apiSuccessExample {json} Success-Response:
	* {
	*	"codestatus": "S",
    *	"message": "Sukses",
    *	"resultdata": {
    *		"nama": "Librano Fauzan",
    *		"email": "rano.dts@gmf-aeroasia.co.id",
    *		"jabatan": "3rd Party - Programmer",
    *		"account_name": "rano.dts",
    *		"account_type": "805306368",
    *		"username": "rano.dts",
    *		"token": "c3130467372ddb6a190bb873c3a73263"
    *	}
	* }
	*
	*
	* @apiErrorExample {json} Error-Response:
	* {
	*	"codestatus": "E",
    *	"message": "Akun LDAP tidak ada!",
    *	"resultdata": [],
	* }
	*/
	public function login()
	{
		try
		{
			$username = $_POST['username'];
			$password = $_POST['password'];

			$manufacture = $_POST['manufacture'];
			$model = $_POST['model'];
			$version = $_POST['version'];
			$sdk = $_POST['sdk'];
			if(!isset($username)) throw new \InvalidArgumentException("Username tidak boleh kosong!", 1);
			if(empty($username)) throw new \InvalidArgumentException("Username tidak boleh kosong!", 1);
			if(!isset($password)) throw new \InvalidArgumentException("Password tidak boleh kosong", 1);
			if(empty($password)) throw new \InvalidArgumentException("Password tidak boleh kosong", 1);

			$ip_ldap = ["192.168.240.57","172.16.100.46"];
			foreach ($ip_ldap as $key => $value) {
				$cek = $this->cek_ldap($username,$password,$value);
				if(!$cek['status']){
					continue;
				}else{
					break;
				}
			}
			if(!$cek['status']) throw new \InvalidArgumentException("Please Check Username/Password!", 1);
			/*$cek = $this->cek_ldap($username,$password);
			if(!$cek['status']){
				for($a=1;$a<=3;$a++){
					$cek = $this->cek_ldap($username,$password);
					if(!$cek['status']){
						sleep(3);
					}else{
						break;
					}
				}
				throw new \InvalidArgumentException("Akun LDAP tidak ada!", 1);
			}*/

			//create token
			$token = $this->createToken($cek['email']);

			unset($cek['status']);
			$param = [
				'nama' => $cek['nama'],
				'email' => $cek['email'],
				'jabatan' => $cek['jabatan'],
				'account_name' => $cek['account_name'],
				'account_type' => $cek['account_type'],
				'username' => $username,
				'password' => md5($password),
				'token' => $token,
				'is_valid' => 1,
				'create_at' => date('Y-m-d H:i:s'),
				'manufacture' => $manufacture,
				'model' => $model,
				'version' => $version,
				'sdk_version' => $sdk,
			];

			$param_update = [
				'is_valid' => 1
			];
			$where_update = [
				'username' => $username,
				'password' => $password,
				'is_valid' => 0
			];

			$update = $this->M_auth->update($param_update,$where_update);
			if(!$update) throw new \InvalidArgumentException("Update login log gagal!", 1);

			$insert = $this->M_auth->insert($param);
			if(!$insert) throw new \InvalidArgumentException("Insert log login gagal!", 1);

			unset($param['password']);
			unset($param['is_valid']);
			unset($param['create_at']);
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Sukses',
				'resultdata'	=> $param,
			];
		}
		catch (\InvalidArgumentException $e)
		{			
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function createToken($email)
	{
		$token = $email."_".date('Y-m-d H:i:s');
		return md5($token);
	}
}
?>