define({
  "name": "Dokumentasi API PHP MDR",
  "version": "0.1.0",
  "description": "API PHP yang berfungsi untuk membantu komunikasi data Aplikasi MDR Mobile ke Aplikasi SAP dan Aplikasi Core MDR",
  "local": "https://dev.gmf-aeroasia.co.id/app_mdr/public/index.php",
  "server": "http://localhost/gmf/app_mdr/public/index.php",
  "title": "API PHP MDR",
  "url": "http://localhost/gmf/app_mdr/public/index.php",
  "sampleUrl": "http://localhost/gmf/app_mdr/public/index.php",
  "template": {
    "withCompare": true,
    "withGenerator": true
  },
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-08-03T07:08:36.739Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
